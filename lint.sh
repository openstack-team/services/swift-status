#!/bin/sh

src_dir=$(pwd -P)

isort --profile black --skip .direnv "$src_dir"
black --exclude '/(\.direnv|\.git)/' "$src_dir"
flake8 --select C,E,F,W,B --ignore E203,E501,W503 --exclude ".direnv,.git" "$src_dir"
